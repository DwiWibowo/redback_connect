(function ($) {
      "use strict";
	  $('#accordionE').on('hide.bs.collapse', function () {
	    // do something…
	    $('.show').parent().find('.btn').removeClass('in');
	  });
	  $('#accordionE').on('shown.bs.collapse', function () {
	    // do something…
	    $('.show').parent().find('.btn').addClass('in');
	  });
	  $('#accordionE').on('show.bs.collapse', function () {
	    // do something…
	    $('.show').parent().find('.btn').addClass('in');
	  });
})(jQuery);