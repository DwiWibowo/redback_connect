// Gulp Plugins
const gulp = require('gulp');
const watch = require('gulp-watch');
const nunjucksRender = require('gulp-nunjucks-render');
const sass = require('gulp-sass');
const prefixer = require('gulp-autoprefixer');
const csso = require('gulp-csso');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const cssbeautify = require('gulp-cssbeautify');
const rename = require('gulp-rename');
// Server
const browserSync = require('browser-sync');
const reload = browserSync.reload;

// Project paths
const path = {
	build: { // Where
		html: './',
		css: './dist/css',
		js: './dist/js'
	},
	src: { // From
		html: './src/html/pages/**/*.+(html|nunjucks)',
		nunjucks: './src/html/templates',
		css: './src/sass/style.sass',
		js: './src/js/index.js'
	},
	watch: { // Watching for changes
		html: './src/html/**/*.+(html|nunjucks)',
		css: './src/sass/**/*.sass',
		js: './src/js/**/*.js'
	}
};

const serverConfig = {
	server: {
		baseDir: "./"
	},
	tunnel: false,
	host: 'localhost',
	port: 9000,
	logPrefix: "gulp_project"
};

// Tasks
gulp.task('html', () => {
	return gulp.src(path.src.html)
		.pipe(nunjucksRender({
			path: [path.src.nunjucks]
		}))
		.pipe(gulp.dest(path.build.html))
		.pipe(reload({
			stream: true
		}));
});

gulp.task('css', () => {
	return gulp.src(path.src.css)
		.pipe(sass({
			outputStyle: 'compressed'
		})).on('error', sass.logError)
		.pipe(rename('style.min.css'))
		.pipe(csso())
		.pipe(prefixer())
		.pipe(gulp.dest(path.build.css))
		.pipe(reload({
			stream: true
		}));
});
gulp.task('cssBeauty', () => {
	return gulp.src(path.src.css)
		.pipe(sass({
			outputStyle: 'compressed'
		})).on('error', sass.logError)
		.pipe(rename('style.css'))
		.pipe(cssbeautify())
		.pipe(prefixer())
		.pipe(gulp.dest(path.build.css))
		.pipe(reload({
			stream: true
		}));
});
gulp.task('js', () => {
	return gulp.src(path.src.js)
		.pipe(webpackStream({
			mode: 'production',
			output: {
				filename: 'bundle.js'
			},
			module: {
				rules: [
					{
						test: /\.(js)$/,
						exclude: /(node_modules)/,
						loader: 'babel-loader',
						query: {
							presets: ['env']
						}
					}
				]
			}
		}))
		.pipe(gulp.dest(path.build.js))
		.pipe(reload({
			stream: true
		}));
});


gulp.task('watch', () => {
	gulp.watch(path.watch.html, gulp.series('html'));
	gulp.watch(path.watch.css, gulp.series('css'));
	gulp.watch(path.watch.js, gulp.series('js'));
});

gulp.task('webserver', () => {
	browserSync(serverConfig);
});

gulp.task('default', gulp.series(
	gulp.series(
		'html',
		'css',		
		'cssBeauty',		
		'js'	
	),
	gulp.parallel(
		'webserver',
		'watch'
	)
));